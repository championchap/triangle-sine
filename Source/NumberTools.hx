package ;

import openfl.geom.Point;

/**
 * ...
 * @author Erik Watson
 */

class NumberTools{

	public static function sineWave(startPos:Point, speed:Float, waveHeight:Int, waveLength:Int):Point {
		var p:Point = startPos;

		p.x += speed;
		p.y = (Math.sin(p.x / waveLength) * waveHeight);

		return p;
	}
	
}