package;

import openfl.display.Sprite;
import openfl.geom.Point;
import openfl.events.Event;
import openfl.display.Stage;
import openfl.Lib;

/**
 * ...
 * @author Erik Watson
 */

class Main extends Sprite {
	
	private var triangles:Array<TriangleSprite>;
	private var numTriangles:Int;
	
	// the sprite that the lines are drawn to 
	private var lines:Sprite;

	// points the lines are made up of 
	private var p0:Array<Point>;
	private var p1:Array<Point>;
	private var p2:Array<Point>;
	
	// used for positioning the triangles on the stage 
	private var tp:Point;
	private var tp2:Point;

	public function new() {
		
		super();
		setup();
	}

	private function setup():Void {
		addEventListener(Event.ENTER_FRAME, onEnterFrame);

		triangles = new Array<TriangleSprite>();

		var t:TriangleSprite;
		var r:Int = 1;
		
		tp = new Point();
		
		p0 = new Array<Point>();
		p1 = new Array<Point>();
		p2 = new Array<Point>();
		
		numTriangles = 150;

		for (i in 0...numTriangles) {
			t = new TriangleSprite();
			t.x = 0;
			t.y = 0;
			t.rotationSpeed = r;
			r++;
			triangles.push(t);
			this.addChild(t);
		}

		lines = new Sprite();
		this.addChild(lines);
	}

	private function onEnterFrame(e:Event):Void {
		update();
		render();
	}

	private function update():Void {
		p0 = [];
		p1 = [];
		p2 = [];
		
		for(t in triangles) {
			t.rotation += t.rotationSpeed / 100;
			
			tp.x = t.x;
			tp.y = t.y;

			tp2 = NumberTools.sineWave(tp, t.rotationSpeed/200, 150, 50);

			t.x = tp2.x;
			t.y = tp2.y + 200;

			p0.push(t.localToGlobal(t.trianglePoints[0]));
			p1.push(t.localToGlobal(t.trianglePoints[1]));
			p2.push(t.localToGlobal(t.trianglePoints[2]));
		}
	}

	private function render():Void {
		lines.graphics.clear();

		lines.graphics.lineStyle(1, 0xffffff, 1, true);

		drawLines(p0);
		drawLines(p1);
		drawLines(p2);
		drawLines(p0);
	}

	private function drawLines(arr:Array<Point>):Void {
		var first:Bool = true;

		for(p in arr){
			if(first == true) {
				lines.graphics.moveTo(p.x, p.y);
				first = false;
			} else {
				lines.graphics.lineTo(p.x, p.y);
			}
		}
	}
	
}