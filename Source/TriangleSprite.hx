package;

import openfl.display.Sprite;
import openfl.geom.Point;

/**
 * ...
 * @author Erik Watson
 */

class TriangleSprite extends Sprite {
	
	public var trianglePoints:Array<Point>;
	private var triangleWidth:Int = 200;
	private var triangleHeight:Int = 173;

	public var rotationSpeed:Int = 0;

	public function new() {
		
		super();
		setup();
	}

	private function setup():Void {
		trianglePoints = new Array<Point>();

		trianglePoints.push(new Point(0, -(triangleHeight/2)));
		trianglePoints.push(new Point(triangleWidth/2, triangleHeight/2));
		trianglePoints.push(new Point(-(triangleWidth/2), triangleHeight/2));

		this.graphics.lineStyle(2, Std.int(Math.random() * 0xff0000), 1, true);
		this.graphics.moveTo(trianglePoints[0].x, trianglePoints[0].y);
		this.graphics.lineTo(trianglePoints[1].x, trianglePoints[1].y);
		this.graphics.lineTo(trianglePoints[2].x, trianglePoints[2].y);
		this.graphics.lineTo(trianglePoints[0].x, trianglePoints[0].y);
	}
	
}