# README #

![trianglesine.png](https://bitbucket.org/repo/zMpLg4/images/2223218967-trianglesine.png)

This is TriangleSine, a simple line drawing experiment made with rotating, connected triangles.


## Requirements ##

* [Haxe](http://haxe.org/)
* [OpenFl](http://www.openfl.org/)

Currently, this project works best in the Neko target.


## Who do I talk to? ##

* [Erik on Twitter](https://twitter.com/championchap)